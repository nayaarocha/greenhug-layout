<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Green Hug</title>
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

      <!-- Bootstrap Core CSS -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>

<body>
   

    <?php include 'menu.php';?>
    <?php include 'header.php';?>
    <?php include 'introgreenhug.php';?>
    <?php include 'intropegadaecologica.php';?>


   

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                     <p class="copyright text-muted small">Projeto desenvolvido no IFRN com apoio do CNPQ</p>
                    <p class="copyright text-muted small">Copyright &copy; Green Hug <?php echo date("Y")?>. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
        var myString = "Abraçe o verde.";
        var myArray = myString.split("");
        var loopTimer;

        function looper() {
          if(myArray.length > 0){
            document.getElementById("animsg").innerHTML += myArray.shift();
          } else {
            clearTimeout(loopTimer);
          }
          loopTimer = setTimeout('looper()', 100);
        }
        looper();


         function ancora(objID) {
            document.getElementById(objID).focus();
        }

    </script>

</body>

</html>
