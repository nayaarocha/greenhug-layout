<!-- Page Content -->


    <div class="content-section-a">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading"><br>O que é o Green Hug?</h2>
                    <p class="lead"> Green Hug é um aplicativo web e mobile focado no turismo sustentável, pra você que se preocupa com os efeitos de suas ações no meio ambiente.</p> 
                    <p class="lead">É também para empresas que trabalham com responsabilidade sustentável e querem divulgar suas ações e serviços.</p>     
                </div>
                
                    <img class="img-responsive3" src="img/logo3.png" alt="">
                
            </div>       
        </div>
        <!-- /.container -->
    </div>