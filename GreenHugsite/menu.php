<!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!--Logotipo GreenHug-->
                <a class="navbar-brand" href="#">
                  
                    <img  src="img/logo2.png">
                     <span class="label label-success beta">Beta</span>

                </a>
            </div>
            
            <!-- Menu Principal-->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#content-section-b">O que é o GreenHug?</a>
                    </li>
                    <li>
                        <button type="button" class="btn btn-success bt-gh">Entrar</button>
                    </li>
                    <li>
                        <button type="button" class="btn btn-success bt-gh">Crie sua conta</button>
                    </li>
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>