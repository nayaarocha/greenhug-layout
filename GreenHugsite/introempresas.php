
     <!-- INTRO HEADER EMPRESAS -->
    <div class="intro-header intro-empresas">

        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="img-responsive2">
                         <img  src= "img/city.png">
                    </div>
                   
                   
                   <!--GrennHug Empresas-->
                    <div class="intro-message intro">
                        <h1>Green Hug para empresas</h1>
                        <h3>Se sua empresa é preocupada com responsabilidade sustentável e oferece serviços
            que realmente espelham essa preocupação, venha para o Green Hug! </h3>
                       
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                            <li>
                                <a href="#" class="btn btn-default btn-lg"><i ></i><span class="network-name">Criar conta</span></a>
                            </li>              
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header EMPRESAS -->
