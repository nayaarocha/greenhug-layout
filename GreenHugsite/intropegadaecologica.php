 <!-- /.content-section-a -->

        <!-- /.container -->

     <div id="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">Pegada ecológica</h2>
                    <p class="lead">Baseado nesse conceito, Green Hug prevê funcionalidades para anular o passivo ambiental causado, por exemplo,</p> 
                    <p class="lead">pelo deslocamento aéreo, promovendo e associando créditos ambientais a serviços </p>
                    <p class="lead">ambientalmente corretos, socialmente responsáveis e que promovam a economia local.</p>
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <img class="img-responsive" src="img/img.png" alt="">
                </div>
            </div>

           

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-b -->